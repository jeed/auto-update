# Project Name

Script for automatically updating itself from a BitBucket Repo.

## Installation

To install run a curl script from the command line.
```
curl -O https://bitbucket.org/jeed/auto-update/raw/master/auto-update.sh
chmod u+x auto-update.sh
```

## Usage

Run from the command line. If it detects that there is a change to the latest
commit it will download and restart the script.


## Contributing

1. Fork it!
2. Create your feature branch: `git checkout -b my-new-feature`
3. Commit your changes: `git commit -am 'Add some feature'`
4. Push to the branch: `git push origin my-new-feature`
5. Submit a pull request :D

## History

TODO: Write history

## Credits

TODO: Write credits

## License

TODO: Write license
