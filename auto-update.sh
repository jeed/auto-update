#!/bin/bash


E_TEMP_CREATE=85                  # No such directory.
E_MISSING_CMDS=80                 # missing external commands
E_UPDATE_BASE_URL_ERR=81          # UPDATE_BASE URL isn't correct
E_BITBUCKET_LOGIN=82              # Credential error logging in
E_BITBUCKET_HOST=83               # couldn't connect to the host
UPDATE_BASE="https://api.bitbucket.org/2.0/repositories/jeed/auto-update"
UPDATE_FILE="https://bitbucket.org/jeed/auto-update"
HASHFILE=/Users/gedesuparsa/dev/auto-test/hash.txt
#UPDATE_BASE="curl "https://dcstash.service.dev:8443/rest/terprise-integration-scripts/commits/master""
#curl --user suparsag:Admin1234 "https://dcstash.service.dev:8443/rest/api/1.0/projects/CSP/repos/enterprise-integration-scripts/commits/master"
my_needed_commands="curl grep sed"

VERBOSE=

err() {
  echo "[$(date +'%Y-%m-%dT%H:%M:%S%z')]: $@" >&2
}

log() {
  if [ $VERBOSE -eq 1 ]; then
    echo "[$(date +'%Y-%m-%dT%H:%M:%S%z')]: $@"
  fi
}

function updateCheck() {
  bitbucket_json=$(curl -k -s "${UPDATE_BASE}/commits/master")
  log $bitbucket_json
  # $? will be 0 if successful
  # $? will be 3 if URL is incorrect
  if [ $? -eq 3 ]; then
    err "Error retrieving from url: ${UPDATE_BASE}"
    exit $E_UPDATE_BASE_URL_ERR
  fi

  # check if connection to host error
  if [ $? -eq 7 ]; then
    err "Error connecting to host: ${UPDATE_BASE}"
    exit $E_BITBUCKET_HOST
  fi

  # need to store output to find login errors
  if [[ $bitbucket_json == {\"errors* ]];
  then
    errmessage=$(echo $bitbucket_json | sed -n -e 's/^.*"message":"\([^"]*\)".*/\1/p')
    err "Error on login: ${errmessage}"
    exit $E_BITBUCKET_LOGIN
  fi

  # we should now be able to get the commit id
  commit_id=$(echo $bitbucket_json | sed -n -e 's/^.*"values": \[{"hash": "\([^"]*\)".*$/\1/p')
  log "Hash: $commit_id"

  # create hashfile if it doesn't exist
  if [ -e ${HASHFILE} ]; then
    hash=$(cat ${HASHFILE})
    # TODO check we can read the file
    if [ $hash = $commit_id ]; then
      log "Hashfile matches: ${hash}"
      # we have a match so do nothing.
    else
      log "Hash diff. Local: ${hash} Remote: ${commit_id}"
      log "Downloading latest to: ${TMPFILE}/master.tar.gz"
      # we havea a mismatch so download, unpack and restart
      curl "${UPDATE_FILE}/get/master.tar.gz" -o "${TMPFILE}/master.tar.gz"
      tar xzf "${TMPFILE}/master.tar.gz" -C ${TMPFILE}
      find ${TMPFILE} -type f -exec mv {} . \;
      exec $0 "$@"
    fi
  else
    echo $commit_id > ${HASHFILE}
    log "Hashfile not found, creating: ${HASHFILE}"
    # TODO check that we have been able to write the file
  fi
}


# create tmp folder
tempfoo=`basename $0`
TMPFILE=`mktemp -d -q /tmp/${tempfoo}.XXXXXX`
if [ $? -ne 0 ]; then
  err "$0: Can't create temp file, exiting..."
  exit $E_TEMP_CREATE
fi


#check for commands need to run
missing_counter=0
for needed_command in $my_needed_commands; do
  if ! hash "$needed_command" >/dev/null 2>&1; then
    err "Command not found in PATH: ${needed_command}"
    ((missing_counter++))
  fi
done

if ((missing_counter > 0)); then
  err "Minimum ${missing_counter} commands are missing in PATH, aborting"
  exit $E_MISSING_CMDS
fi


# check latest updates

while getopts "hv" OPTION
do
  case $OPTION in
    h )
      usage
      exit 1
      ;;
    v )
      VERBOSE=1
      ;;
  esac
done

updateCheck
